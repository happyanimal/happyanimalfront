import { Stomp } from '@stomp/stompjs';
import { useEffect } from 'react';

import {
    clearNotification,
    setNotification,
} from './components/molecules/notifications/reducers/notification';
import { useAppDispatch, useAppSelector } from './hooks/defaulthooks';

const useNotifications = () => {
    const dispatch = useAppDispatch();
    const notification = useAppSelector(
        (state) => state.notification.currentNotification
    );
    useEffect(() => {
        if (notification.isActive) {
            setTimeout(() => {
                dispatch(clearNotification(false));
            }, 5000);
        }
    }, [notification]);

    useEffect(() => {
        const role = localStorage.getItem('userRole');
        if (role === '[EMPLOYEE]') {
            const socket = new WebSocket('ws://localhost:8080/api/chat');
            const ws = Stomp.over(socket);
            ws.reconnectDelay = 5000;
            const userId = localStorage.getItem('userId');
            ws.connect({}, (frame: any) => {
                ws.subscribe(`/topic/${userId}`, (message) => {
                    dispatch(
                        setNotification({
                            type: 'info',
                            text: message.body,
                            isActive: true,
                        })
                    );
                });
            });
        }
    });

    return { notification };
};

export default useNotifications;
