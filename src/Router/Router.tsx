import AddAnimalPage from 'components/pages/addAnimalPage/UI/addAnimalPage';
import AddEmployeePage from 'components/pages/addEmployeePage/UI/addEmployeePage';
import AddTaskPage from 'components/pages/addTaskPage/UI/addTaskPage';
import AnimalCardPage from 'components/pages/animalCardPage/UI/animalCardPage';
import EmployeeCardPage from 'components/pages/employeeCardPage/UI/employeeCardPage';
import EmployeesPage from 'components/pages/employeesPage/UI/employeesPage';
import ExhibitionsPage from 'components/pages/exhibitionsPage/UI/exhibitionsPage';
import InspectionsPage from 'components/pages/inspectionsPage/UI/inspectionsPage';
import ReportPage from 'components/pages/reportPage/UI/reportPage';
import ReportsPage from 'components/pages/reportsPage/UI/reportsPage';
import TaskCardPage from 'components/pages/taskCardPage/UI/taskCardPage';
import TasksPage from 'components/pages/tasksPage/UI/tasksPage';
import { Route, Routes } from 'react-router-dom';

import AnimalsPage from '../components/pages/animalsPage/UI/animalsPage';

export function RoutesPaths() {
    return (
        <Routes>
            <Route path="/animals" element={<AnimalsPage />} />
            <Route path="/animals/add" element={<AddAnimalPage />} />
            <Route path="/animals/:id" element={<AnimalCardPage />} />
            <Route path="/employees/add" element={<AddEmployeePage />} />
            <Route path="/employees" element={<EmployeesPage />} />
            <Route path="/employees/:id" element={<EmployeeCardPage />} />
            <Route path="/tasks" element={<TasksPage />} />
            <Route path="/inspections" element={<InspectionsPage />} />
            <Route path="/exhibitions" element={<ExhibitionsPage />} />
            <Route path="/tasks/:id" element={<TaskCardPage />} />
            <Route path="/tasks/add/:id" element={<AddTaskPage />} />
            <Route path="/reports" element={<ReportsPage />} />
            <Route path="/reports/:id" element={<ReportPage />} />
        </Routes>
    );
}
