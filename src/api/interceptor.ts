import {
    FetchArgs,
    FetchBaseQueryError,
    FetchBaseQueryMeta,
} from '@reduxjs/toolkit/dist/query';
import {
    BaseQueryApi,
    BaseQueryFn,
    QueryReturnValue,
} from '@reduxjs/toolkit/dist/query/baseQueryTypes';
import { setUserAuthecationInfo } from 'components/molecules/authorizationBlock/reducers/authorization';
import { setNotification } from 'components/molecules/notifications/reducers/notification';
import { setIsAuthorization } from 'components/pages/authorizationPage/reducers/authorization';

export const interseptor = async (
    result: QueryReturnValue<unknown, FetchBaseQueryError, FetchBaseQueryMeta>,
    args: string | FetchArgs,
    baseQuery: BaseQueryFn<
        string | FetchArgs,
        unknown,
        FetchBaseQueryError,
        {},
        FetchBaseQueryMeta
    >,
    api: BaseQueryApi,
    extraOptions: {}
) => {
    if (result.error && result.error.status === 401) {
        localStorage.removeItem('isAuth');
        api.dispatch(setUserAuthecationInfo({ token: null }));
        api.dispatch(setIsAuthorization({ isAuth: false }));
        api.dispatch(
            setNotification({
                type: 'error',
                text: `Ошибка авторизации, попробуйте еще раз`,
                isActive: true,
            })
        );
    }
    return result;
};
