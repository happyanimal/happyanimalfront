import './App.css';

import Notifications from './components/molecules/notifications/notifications';
import useNotifications from './useNotifications';

function App() {
    const { notification } = useNotifications();

    return (
        <div className="App">
            <Notifications
                open={notification.isActive}
                text={notification.text}
                type={notification.type}
            />
        </div>
    );
}

export default App;
