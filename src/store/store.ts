import { configureStore } from '@reduxjs/toolkit';
import { emptySplitApi } from 'api/api';

import authenticatedUserInfo from '../components/molecules/authorizationBlock/reducers/authorization';
import notification from '../components/molecules/notifications/reducers/notification';
import visible from '../components/organisms/animalCard/reducers/visible';
import authorization from '../components/pages/authorizationPage/reducers/authorization';

export const store = configureStore({
    reducer: {
        notification,
        authenticatedUserInfo,
        authorization,
        visible,
        [emptySplitApi.reducerPath]: emptySplitApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(emptySplitApi.middleware),
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
