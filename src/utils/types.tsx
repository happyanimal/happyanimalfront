export interface IUser {
    age: number;
    id: number;
    lastName: string;
    login: string;
    name: string;
    patronymic: string;
    userRole: string;
}

export const roles = {
    ADMIN: 'Админ',
    EMPLOYEE: 'Сотрудник',
    VETERINARIAN: 'Ветеринар',
    SUPER_ADMIN: 'Супер Админ',
};
