export const getSearchParams = (serachLocation: string) => {
    const s = serachLocation.slice(1).split('&');
    return s.reduce(
        (previousValue: { [key: string]: string }, currentValue: string) => {
            const a: Array<string> = currentValue.split('=');
            const key = a[0];
            const value = a[1];
            previousValue[key] = value;
            return previousValue;
        },
        {}
    );
};
