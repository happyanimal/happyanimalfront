import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getKinds: build.query({
            query: () => ({
                url: 'api/animalKinds/only-kinds',
                method: 'GET',
            }),
        }),
        getStatuses: build.query({
            query: () => ({
                url: 'api/animals/states',
                method: 'GET',
            }),
        }),
        getUsers: build.query({
            query: () => ({
                url: 'api/users',
                method: 'GET',
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useGetKindsQuery, useGetStatusesQuery, useGetUsersQuery } =
    extendedApi;
