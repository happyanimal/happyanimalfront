import styles from './addAndOptionButtons.module.css';

type AddAndOptionButtonsProps = {
    handleAdd?: () => void;
    handleOpenOptions?: () => void;
    addImg?: string;
    optionalImg?: string;
};

export function AddAndOptionButtons({
    handleAdd,
    handleOpenOptions,
    addImg,
    optionalImg,
}: AddAndOptionButtonsProps) {
    return (
        <div className={styles.buttonsWrapper}>
            {addImg && (
                <button className={styles.buttonImg} onClick={handleAdd}>
                    <img src={addImg} alt="add animal" className={styles.img} />
                </button>
            )}
            {optionalImg && (
                <button
                    className={styles.buttonImg}
                    onClick={handleOpenOptions}
                >
                    <img
                        src={optionalImg}
                        alt="option"
                        className={styles.img}
                    />
                </button>
            )}
        </div>
    );
}
