import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getRepeats: build.query({
            query: () => ({
                url: '/api/tasks/repeat-types',
                method: 'GET',
            }),
        }),
        getAnimal: build.query({
            query: (id) => ({
                url: `/api/animals/${id}`,
                method: 'GET',
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useGetRepeatsQuery, useLazyGetAnimalQuery } = extendedApi;
