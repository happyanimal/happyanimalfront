import { Button } from 'components/atoms/button/button';
import { DropDown } from 'components/atoms/dropdown/dropdown';
import { Input } from 'components/atoms/input/input';
import { TextArea } from 'components/atoms/textarea/textarea';
import { DateTimePicker } from 'components/molecules/DateTimePicker/UI/DateTimePicker';
import { Dayjs } from 'dayjs';
import { useEffect } from 'react';
import { useLocation } from 'react-router';

import {
    useGetRepeatsQuery,
    useLazyGetAnimalQuery,
} from '../requests/requests';
import styles from './addTaskCard.module.css';

type AddAnimalCardProps = {
    type: string;
    handleChangeType: (e: React.ChangeEvent<HTMLInputElement>) => void;
    repeatCount: string;
    handleChangeRepeatCount: (editValue: string | number) => void;
    date: Dayjs | null;
    handleChangeDate: (value: Dayjs | null) => void;
    note: string;
    handleChangeNote: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
    handleAddTask: () => void;
};

export function AddTaskCard({
    type,
    handleChangeType,
    repeatCount,
    handleChangeRepeatCount,
    date,
    handleChangeDate,
    note,
    handleChangeNote,
    handleAddTask,
}: AddAnimalCardProps) {
    const { data: repeats } = useGetRepeatsQuery({});
    const location = useLocation();
    const currentId = location.pathname.split('/')[3];
    const [getAnimal, params] = useLazyGetAnimalQuery();
    useEffect(() => {
        if (currentId) getAnimal(currentId);
    }, [currentId]);

    return (
        <div className={styles.wrapperOrders}>
            <div className={styles.fieldsWrapper}>
                <Input
                    title="Тип задачи"
                    value={type}
                    onChange={handleChangeType}
                />
                <div className={styles.field}>
                    <DropDown
                        label="Количество повторений"
                        value={repeatCount}
                        onChange={handleChangeRepeatCount}
                        source={repeats?.map((el: string) => ({
                            value: el,
                            text: el,
                        }))}
                    />
                </div>
            </div>
            <div className={styles.fieldsWrapper}>
                <Input
                    title="Животное"
                    value={params?.data?.name ?? ''}
                    disabled
                />
                <div className={styles.fieldVisible}>
                    <DateTimePicker
                        withOutPadding
                        title="Время выполнения"
                        date={date}
                        handleChangeDate={handleChangeDate}
                    />
                </div>
            </div>
            <div className={styles.textAreaWrapper}>
                <TextArea
                    title="Примечание"
                    value={note}
                    onChange={handleChangeNote}
                />
            </div>
            <div className={styles.buttonWrapper}>
                <Button onClick={handleAddTask}>Добавить</Button>
            </div>
        </div>
    );
}
