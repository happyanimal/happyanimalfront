import styles from './editAndOptionButtons.module.css';

type EditAndOptionButtonsProps = {
    handleEdit?: () => void;
    handleCheck?: () => void;
    handleOpenOptions?: () => void;
    editImg?: string;
    optionalImg?: string;
    checkImg?: string;
};

export function EditAndOptionButtons({
    handleEdit,
    handleCheck,
    handleOpenOptions,
    editImg,
    optionalImg,
    checkImg,
}: EditAndOptionButtonsProps) {
    return (
        <div className={styles.buttonsWrapper}>
            <button className={styles.buttonImg} onClick={handleCheck}>
                <img src={checkImg} alt="check" width="30" />
            </button>
            <button className={styles.buttonImg} onClick={handleEdit}>
                <img src={editImg} alt="edit" width="30" />
            </button>
            <button className={styles.buttonImg} onClick={handleOpenOptions}>
                <img src={optionalImg} alt="options" width="35" />
            </button>
        </div>
    );
}
