import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface IsVisibleBlockInterface {
    isVisible: boolean;
}

const initialState: IsVisibleBlockInterface = {
    isVisible: false,
};

export const authUserInfo = createSlice({
    name: 'visible',
    initialState,
    reducers: {
        setVisible: (state, action: PayloadAction<IsVisibleBlockInterface>) => {
            state.isVisible = action.payload.isVisible;
        },
    },
});

export const { setVisible } = authUserInfo.actions;

export default authUserInfo.reducer;
