import {
    useGetEmployeesQuery,
    useGetStatusesQuery,
} from 'components/organisms/addAnimalCard/requests/requests';
import { useAppDispatch, useAppSelector } from 'hooks/defaulthooks';
import { useEffect, useRef } from 'react';
import { useLocation } from 'react-router';

import { setVisible } from '../reducers/visible';
import { useLazyGetAnimalQuery } from '../requests/requests';

const useAnimalCard = () => {
    const dispatch = useAppDispatch();
    const isVisible = useAppSelector((store) => store.visible.isVisible);
    const ref = useRef<HTMLUListElement>(null);
    const locationPath = useLocation();
    const currentId = locationPath.pathname.split('/')[2];

    const handleClickOutside = (event: any) => {
        if (ref.current && !ref.current.contains(event.target)) {
            dispatch(setVisible({ isVisible: false }));
        }
    };
    const handleChangeVisibleBlock = () => {
        dispatch(setVisible({ isVisible: true }));
    };

    useEffect(() => {
        document.addEventListener('click', handleClickOutside, true);
        return () => {
            document.removeEventListener('click', handleClickOutside, true);
        };
    });

    const [getAnimal, animal] = useLazyGetAnimalQuery();
    useEffect(() => {
        if (currentId) {
            getAnimal(currentId);
        }
    }, [currentId, getAnimal]);

    const { data: users } = useGetEmployeesQuery({});
    const { data: statuses } = useGetStatusesQuery({});
    const role = localStorage.getItem('userRole');

    const handleAddTask = () => {
        dispatch(setVisible({ isVisible: false }));
    };
    return {
        isVisible,
        handleChangeVisibleBlock,
        animal,
        users,
        statuses,
        ref,
        role,
        handleAddTask,
    };
};

export default useAnimalCard;
