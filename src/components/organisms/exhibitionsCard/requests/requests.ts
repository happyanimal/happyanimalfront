import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getExhibitions: build.query({
            query: (date) => ({
                url: `api/exhibitions/date/${date}`,
                method: 'GET',
            }),
        }),
        getAnimalsForExhibitions: build.query({
            query: () => ({
                url: `/api/animals/permitted`,
                method: 'GET',
            }),
        }),
        addAnimalForExhibition: build.mutation({
            query: ({ id, ...params }) => ({
                url: `/api/exhibitions/${id}`,
                method: 'PUT',
                params: {
                    ...params,
                },
            }),
        }),
    }),
    overrideExisting: false,
});

export const {
    useLazyGetExhibitionsQuery,
    useGetAnimalsForExhibitionsQuery,
    useAddAnimalForExhibitionMutation,
} = extendedApi;
