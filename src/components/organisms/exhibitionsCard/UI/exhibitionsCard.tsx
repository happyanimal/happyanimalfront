import { Button } from 'components/atoms/button/button';
import { DropDown } from 'components/atoms/dropdown/dropdown';
import { AnimalLine } from 'components/molecules/animalLine/UI/animalLine';
import { DatePicker } from 'components/molecules/DatePicker/UI/DatePicker';

import checkImg from '../../animalCard/assets/check.png';
import useExhibitionsCard from '../logic/useExhibitionsCard';
import styles from './exhibitionsCard.module.css';

export function ExhibitionsCard() {
    const {
        isVisible,
        date,
        handleChangeDate,
        handleGetExhibitions,
        params,
        animals,
        animal,
        handleChangeAnimal,
        handleAddAnimalForExhibition,
        handleShowAnimalsDropDown,
        handleGoBack,
    } = useExhibitionsCard();

    return (
        <div className={styles.wrapperOrders}>
            <div className={styles.wrapperFields}>
                <DatePicker date={date} handleChangeDate={handleChangeDate} />
                {isVisible && (
                    <div className={styles.container}>
                        <DropDown
                            label="Выберите животное доступное для выставки"
                            value={animal}
                            onChange={handleChangeAnimal}
                            source={animals?.map((el: any) => ({
                                value: el.id,
                                text: el.name,
                            }))}
                        />
                        <button
                            className={styles.buttonImg}
                            onClick={handleAddAnimalForExhibition}
                        >
                            <img
                                src={checkImg}
                                alt="edit"
                                className={styles.imgCheck}
                            />
                        </button>
                    </div>
                )}
            </div>
            <div className={styles.wrapperBox}>
                <div className={styles.buttonWrapper}>
                    {!isVisible && (
                        <Button onClick={handleShowAnimalsDropDown}>
                            Добавить животных
                        </Button>
                    )}
                    {!isVisible && (
                        <div className={styles.button}>
                            <Button onClick={handleGetExhibitions}>
                                Получить список животных
                            </Button>
                        </div>
                    )}
                    {isVisible && (
                        <Button onClick={handleGoBack}>Вернуться назад</Button>
                    )}
                </div>
                {!isVisible && (
                    <div className={styles.animalWrapper}>
                        {params?.currentData?.animals?.map((el: any) => (
                            <AnimalLine
                                imgPath={el.animalKindDto.pic}
                                key={`AnimalInspection${el.id}`}
                                name={el.name}
                                location={el.animalKindDto.kind}
                                id={el.id}
                            />
                        ))}
                    </div>
                )}{' '}
            </div>
        </div>
    );
}
