import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getInspections: build.query({
            query: (date) => ({
                url: `api/inspection/date/${date}`,
                method: 'GET',
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useLazyGetInspectionsQuery } = extendedApi;
