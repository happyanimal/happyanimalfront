import { Button } from 'components/atoms/button/button';
import { AnimalLine } from 'components/molecules/animalLine/UI/animalLine';
import { DatePicker } from 'components/molecules/DatePicker/UI/DatePicker';

import useInspectionCard from '../logic/useInspectionCard';
import styles from './inspectionCard.module.css';

export function InspectionCard() {
    const { date, handleChangeDate, handleGetInspection, params } =
        useInspectionCard();
    return (
        <div className={styles.wrapperOrders}>
            <DatePicker date={date} handleChangeDate={handleChangeDate} />
            <div className={styles.wrapperBox}>
                <div className={styles.buttonWrapper}>
                    <Button onClick={handleGetInspection}>
                        Получить список
                    </Button>
                </div>
                <div className={styles.animalWrapper}>
                    {params?.currentData?.animalList?.map((el: any) => (
                        <AnimalLine
                            imgPath={el.animalKindDto.pic}
                            key={`AnimalInspection${el.id}`}
                            name={el.name}
                            location={el.animalKindDto.kind}
                            id={el.id}
                        />
                    ))}
                </div>
            </div>
        </div>
    );
}
