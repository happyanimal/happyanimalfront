import { setNotification } from 'components/molecules/notifications/reducers/notification';
import dayjs, { Dayjs } from 'dayjs';
import { useAppDispatch } from 'hooks/defaulthooks';
import { useEffect, useState } from 'react';

import { useLazyGetInspectionsQuery } from '../requests/requests';

const useInspectionCard = () => {
    const dispatch = useAppDispatch();
    const [getInspections, params] = useLazyGetInspectionsQuery<any>();
    const [date, setDate] = useState<Dayjs | null>(dayjs());

    const handleChangeDate = (value: Dayjs | null) => {
        setDate(value);
    };
    const handleGetInspection = async () => {
        await getInspections(dayjs(date).format('YYYY-MM-DD'));
    };

    useEffect(() => {
        if (params.isError) {
            dispatch(
                setNotification({
                    type: 'error',
                    text: params.error.data.description,
                    isActive: true,
                })
            );
        }
    }, [params]);
    return { date, handleChangeDate, handleGetInspection, params };
};

export default useInspectionCard;
