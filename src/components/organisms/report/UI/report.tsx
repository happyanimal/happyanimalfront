import { Pagination } from '@mui/material';
import { Button } from 'components/atoms/button/button';
import { DatePicker } from 'components/molecules/DatePicker/UI/DatePicker';
import { ReportLine } from 'components/molecules/reportLine/UI/reportLine';
import dayjs from 'dayjs';

import useReport from '../logic/useReport';
import styles from './report.module.css';

export function Report() {
    const {
        fromDate,
        toDate,
        handleChangeFromDate,
        handleChangeToDate,
        handleGetInspection,
        params,
        currentPage,
        handleChangePage,
    } = useReport();
    return (
        <>
            <div className={styles.wrapperOrders}>
                <div className={styles.datesWrapper}>
                    <DatePicker
                        withOutPadding
                        title="Начало периода"
                        date={fromDate}
                        handleChangeDate={handleChangeFromDate}
                    />
                    <div className={styles.datePickerContainer}>
                        <DatePicker
                            withOutPadding
                            title="Конец периода"
                            date={toDate}
                            handleChangeDate={handleChangeToDate}
                        />
                    </div>
                </div>
                <div className={styles.wrapperBox}>
                    <div className={styles.buttonWrapper}>
                        <Button onClick={handleGetInspection}>
                            Получить отчеты
                        </Button>
                    </div>
                </div>
            </div>
            <div className={styles.animalWrapper}>
                {params?.currentData?.content.map((el: any) => (
                    <ReportLine
                        key={`AnimalReport${el.id}`}
                        period={dayjs(el.completedDateTime).format(
                            'DD-MM-YYYY HH:mm'
                        )}
                        taskType={el.taskType}
                        id={el.id}
                        taskId={el.taskId}
                    />
                ))}
            </div>
            <div className={styles.wrapperPagination}>
                <Pagination
                    count={params?.currentData?.totalPages}
                    page={currentPage + 1}
                    onChange={handleChangePage}
                />
            </div>
        </>
    );
}
