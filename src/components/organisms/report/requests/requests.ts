import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getReports: build.query({
            query: ({ startDate, endDate, page, size }) => ({
                url: `api/taskLog`,
                method: 'GET',
                params: {
                    page,
                    size,
                    startDate,
                    endDate,
                },
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useLazyGetReportsQuery } = extendedApi;
