import { setNotification } from 'components/molecules/notifications/reducers/notification';
import dayjs, { Dayjs } from 'dayjs';
import { useAppDispatch } from 'hooks/defaulthooks';
import { useEffect, useState } from 'react';

import { useLazyGetReportsQuery } from '../requests/requests';

const useReport = () => {
    const dispatch = useAppDispatch();
    const [getReports, params] = useLazyGetReportsQuery<any>();
    const [fromDate, setFromDate] = useState<Dayjs | null>(dayjs());
    const [toDate, setToDate] = useState<Dayjs | null>(dayjs());
    const [currentPage, setCurrentPage] = useState(0);

    const handleChangePage = (event: any, pageNumber: number) => {
        setCurrentPage(pageNumber - 1);
    };

    const handleChangeFromDate = (value: Dayjs | null) => {
        setFromDate(value);
    };
    const handleChangeToDate = (value: Dayjs | null) => {
        setToDate(value);
    };
    const handleGetInspection = async () => {
        await getReports({
            startDate: dayjs(fromDate).format('YYYY-MM-DD'),
            endDate: dayjs(toDate).format('YYYY-MM-DD'),
            page: currentPage,
            size: 5,
        });
    };

    useEffect(() => {
        if (currentPage > 0) {
            getReports({
                startDate: dayjs(fromDate).format('YYYY-MM-DD'),
                endDate: dayjs(toDate).format('YYYY-MM-DD'),
                page: currentPage,
                size: 5,
            });
        }
    }, [currentPage]);

    useEffect(() => {
        if (params.isError) {
            dispatch(
                setNotification({
                    type: 'error',
                    text: params.error.data.description,
                    isActive: true,
                })
            );
        }
    }, [params]);
    return {
        handleChangePage,
        currentPage,
        fromDate,
        toDate,
        handleChangeFromDate,
        handleChangeToDate,
        handleGetInspection,
        params,
    };
};

export default useReport;
