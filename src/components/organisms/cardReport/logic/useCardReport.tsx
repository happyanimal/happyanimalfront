import { useEffect } from 'react';
import { useLocation } from 'react-router';

import {
    useLazyGetReportQuery,
    useLazyGetUserQuery,
} from '../requests/requests';

const useCardReport = () => {
    const location = useLocation();
    const currentId = location.pathname.split('/')[2];
    const [getUser, params] = useLazyGetUserQuery();
    const [getReport, paramsReport] = useLazyGetReportQuery();

    useEffect(() => {
        if (currentId) {
            getReport(currentId);
        }
    }, [currentId]);

    useEffect(() => {
        if (paramsReport.isSuccess && paramsReport.data.userId) {
            getUser(paramsReport.data.userId);
        }
    }, [paramsReport]);
    return {
        params,
        paramsReport,
    };
};

export default useCardReport;
