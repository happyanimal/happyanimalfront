import { Menu } from 'components/molecules/menu/UI/menu';

import styles from './header.module.css';

export function Header() {
    return (
        <header className={styles.wrapper}>
            <div className="mainTitle">
                <span className={styles.firstTitle}>Happy</span>
                <span className={styles.secondTitle}>Animals</span>
            </div>
            <Menu />
        </header>
    );
}
