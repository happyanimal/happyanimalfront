import { Box, Modal } from '@mui/material';
import { Button } from 'components/atoms/button/button';
import { DatePicker } from 'components/molecules/DatePicker/UI/DatePicker';
import { Dayjs } from 'dayjs';

import styles from './addAnimalInspectionModal.module.css';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '1px solid #000',
    boxShadow: 24,
    p: 4,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
};

type AddAnimalInspectionModalProps = {
    isOpen: boolean;
    handleClose: () => void;
    dateForInspections: Dayjs | null;
    handleChangeDateForInspections: (value: Dayjs | null) => void;
    handleAddInspections: () => void;
};

export function AddAnimalInspectionModal({
    isOpen,
    handleClose,
    dateForInspections,
    handleChangeDateForInspections,
    handleAddInspections,
}: AddAnimalInspectionModalProps) {
    return (
        <Modal
            open={isOpen}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <div className={styles.title}>
                    Выберите дату для осмотра ветеринаром
                </div>
                <DatePicker
                    disabled={false}
                    date={dateForInspections}
                    handleChangeDate={handleChangeDateForInspections}
                />
                <div className={styles.buttonInspections}>
                    <Button onClick={handleAddInspections}>
                        Отправить к ветеринару
                    </Button>
                </div>
            </Box>
        </Modal>
    );
}
