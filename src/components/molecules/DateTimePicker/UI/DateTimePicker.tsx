import { DesktopDateTimePicker } from '@mui/x-date-pickers';
import { Input } from 'components/atoms/input/input';
import dayjs, { Dayjs } from 'dayjs';

import styles from './DateTimePicker.module.css';

type DateTimePickerInfoProps = {
    title?: string;
    date?: Dayjs | null;
    handleChangeDate: (value: Dayjs | null) => void;
    withOutPadding?: boolean;
    disabled?: boolean;
};

export function DateTimePicker({
    title,
    date,
    handleChangeDate,
    withOutPadding,
    disabled = true,
}: DateTimePickerInfoProps) {
    return (
        <DesktopDateTimePicker
            value={date}
            onChange={handleChangeDate}
            renderInput={({ inputRef, inputProps, InputProps }) => (
                <div
                    ref={inputRef}
                    className={`${styles.datePickerWrapper} ${
                        withOutPadding ? styles.withOutPadding : ''
                    }`}
                >
                    <Input
                        disabled={disabled}
                        onChange={() => {}}
                        title={title}
                        value={dayjs(date).format('DD-MM-YYYY HH:mm')}
                    />
                    {InputProps?.endAdornment}
                </div>
            )}
        />
    );
}
