import styles from './block.module.css';

type BlockProps = {
    title?: string;
    subtitle?: string;
};

export function Block({ title, subtitle }: BlockProps) {
    return (
        <div className={styles.block}>
            <div className={styles.mainWrapperTitle}>{title}</div>
            <div className={styles.mainWrapperSubtitle}>{subtitle}</div>
        </div>
    );
}
