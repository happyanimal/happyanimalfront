import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface INotification {
    isActive: boolean;
    type: 'error' | 'warning' | 'info' | 'success' | undefined;
    text: string;
}

const initialState = {
    currentNotification: <INotification>{
        isActive: false,
        text: '',
        type: undefined,
    },
};

export const counterSlice = createSlice({
    name: 'notification',
    initialState,
    reducers: {
        setNotification: (state, action: PayloadAction<INotification>) => {
            state.currentNotification = action.payload;
        },
        clearNotification: (state, action: PayloadAction<boolean>) => {
            state.currentNotification.isActive = action.payload;
        },
    },
});

export const { setNotification, clearNotification } = counterSlice.actions;

export default counterSlice.reducer;
