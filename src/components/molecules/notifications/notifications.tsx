import MuiAlert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';

type NotificationsProps = {
    text?: string;
    open?: boolean;
    handleClose?: () => void;
    type: 'error' | 'warning' | 'info' | 'success' | undefined;
    delay?: number;
};

export default function Notifications({
    text,
    open,
    handleClose,
    type,
    delay,
}: NotificationsProps) {
    return (
        <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={open}
            autoHideDuration={delay}
            onClose={handleClose}
        >
            <MuiAlert variant="filled" severity={type} sx={{ width: '100%' }}>
                {text}
            </MuiAlert>
        </Snackbar>
    );
}
