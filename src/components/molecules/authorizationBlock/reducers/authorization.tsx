import { createSlice, PayloadAction } from '@reduxjs/toolkit';
/**
 * Хранилище данных пользователя
 *
 * @return {setUserAuthecationInfo} - функция смены информации о текущем пользователе
 *
 */

export interface UserAuthenctionStoreInterface {
    token: number | string | null;
}

const initialState: UserAuthenctionStoreInterface = {
    token: null,
};

export const authUserInfo = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        setUserAuthecationInfo: (
            state,
            action: PayloadAction<UserAuthenctionStoreInterface>
        ) => {
            state.token = action.payload.token;
        },
    },
});

export const { setUserAuthecationInfo } = authUserInfo.actions;

export default authUserInfo.reducer;
