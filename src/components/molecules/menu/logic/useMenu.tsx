import { setUserAuthecationInfo } from 'components/molecules/authorizationBlock/reducers/authorization';
import { setIsAuthorization } from 'components/pages/authorizationPage/reducers/authorization';
import { useAppDispatch } from 'hooks/defaulthooks';
import { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router';

import { useLogoutUserMutation } from '../requests/requests';

const useMenu = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const location = useLocation();
    const [currentActiveLink, setCurrentActiveLink] = useState([
        { name: '/animals', active: false },
        {
            name: '/employees',
            active: false,
        },
        { name: '/tasks', active: false },
        { name: '/inspections', active: false },
        { name: '/exhibitions', active: false },
        { name: '/reports', active: false },
    ]);

    useEffect(() => {
        const array = currentActiveLink.map((el: any) => {
            let active = false;
            if (el.name === location.pathname) {
                active = true;
            }
            return { name: el.name, active };
        });
        setCurrentActiveLink(array);
    }, [location.pathname]);

    const [logout, params] = useLogoutUserMutation();

    const onLogout = async () => {
        await logout({});
        navigate('/');
    };

    useEffect(() => {
        if (params.isSuccess) {
            localStorage.removeItem('isAuth');
            localStorage.removeItem('role');
            localStorage.removeItem('token');
            dispatch(setIsAuthorization({ isAuth: true }));
            dispatch(setUserAuthecationInfo({ token: null }));
        }
    }, [params]);

    const role = localStorage.getItem('userRole');

    return {
        currentActiveLink,
        onLogout,
        role,
    };
};

export default useMenu;
