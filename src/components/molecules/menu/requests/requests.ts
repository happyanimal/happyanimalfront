import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        logoutUser: build.mutation({
            query: () => ({
                url: `/api/auth/logout`,
                method: 'POST',
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useLogoutUserMutation } = extendedApi;
