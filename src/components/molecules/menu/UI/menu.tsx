import { MenuItem } from 'components/atoms/menuItem/UI/menuItem';
import { Link } from 'react-router-dom';

import reportImg from '../assets/analytics.png';
import tasksImg from '../assets/clipboard.png';
import employeeImg from '../assets/employees.png';
import exhibitsImg from '../assets/exhibit.png';
import animalsImg from '../assets/paw.png';
import telegramImg from '../assets/telegram.png';
import vetImg from '../assets/vet.png';
import useMenu from '../logic/useMenu';
import styles from './menu.module.css';

export function Menu() {
    const { currentActiveLink, onLogout, role } = useMenu();
    const userId = localStorage.getItem('userId');
    return (
        <div className={styles.menu}>
            <div>
                {(role === 'ADMIN' ||
                    role === 'SUPER_ADMIN' ||
                    role === 'EMPLOYEE') && (
                    <Link to="/animals">
                        <MenuItem
                            text="Животные"
                            active={currentActiveLink[0].active}
                            img={animalsImg}
                        />
                    </Link>
                )}
                {(role === 'ADMIN' || role === 'SUPER_ADMIN') && (
                    <Link to="employees">
                        <MenuItem
                            text="Сотрудники"
                            active={currentActiveLink[1].active}
                            img={employeeImg}
                        />
                    </Link>
                )}
                {(role === 'ADMIN' ||
                    role === 'SUPER_ADMIN' ||
                    role === 'EMPLOYEE') && (
                    <Link to="tasks">
                        <MenuItem
                            text="Задачи"
                            active={currentActiveLink[2].active}
                            img={tasksImg}
                        />
                    </Link>
                )}
                {(role === 'ADMIN' ||
                    role === 'SUPER_ADMIN' ||
                    role === 'VETERINARIAN') && (
                    <Link to="inspections">
                        <MenuItem
                            text="Осмотры"
                            active={currentActiveLink[3].active}
                            img={vetImg}
                        />
                    </Link>
                )}
                {(role === 'ADMIN' || role === 'SUPER_ADMIN') && (
                    <Link to="exhibitions">
                        <MenuItem
                            text="Выставки"
                            active={currentActiveLink[4].active}
                            img={exhibitsImg}
                        />
                    </Link>
                )}
                {(role === 'ADMIN' || role === 'SUPER_ADMIN') && (
                    <Link to="reports">
                        <MenuItem
                            text="Отчеты"
                            active={currentActiveLink[5].active}
                            img={reportImg}
                        />
                    </Link>
                )}
                {role === 'EMPLOYEE' && (
                    <a
                        href={`https://t.me/happy_animal_bot?start=${userId}`}
                        target="_blank"
                        rel="noreferrer"
                    >
                        <MenuItem text="Telegram" img={telegramImg} />
                    </a>
                )}
            </div>
            <Link to="/">
                <div
                    className={styles.exit}
                    onClick={onLogout}
                    onKeyPress={onLogout}
                    role="button"
                    tabIndex={0}
                >
                    <MenuItem text="Выход" />
                </div>
            </Link>
        </div>
    );
}
