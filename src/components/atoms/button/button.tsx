import { memo } from 'react';

import styles from './button.module.css';

type ButtonProps = {
    children?: React.ReactNode;
    disabled?: boolean;
    onClick?: () => void;
    theme?: 'default';
    type?: 'submit' | 'reset' | 'button';
};
function CustomButton({
    onClick,
    disabled = false,
    children = '',
    theme = 'default',
    type = 'button',
}: ButtonProps) {
    return (
        <button
            className={`${styles.button} ${styles[theme]} upperTitle`}
            onClick={onClick}
            disabled={disabled}
            type={type}
        >
            {children}
        </button>
    );
}

export const Button = memo(CustomButton);
