import { memo } from 'react';

import styles from './textarea.module.css';

type TextAreaProps = {
    value?: string;
    onChange?: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
    defaultValue?: string;
    disabled?: boolean;
    title?: string;
};

function CustomTextArea({
    value,
    onChange,
    disabled = false,
    defaultValue,
    title = '',
}: TextAreaProps) {
    return (
        <div className={styles.wrapper}>
            <div className={styles.title}>{title}</div>
            <textarea
                value={value}
                defaultValue={defaultValue}
                onChange={onChange}
                disabled={disabled}
                className={`${styles.textarea} Body1`}
            />
        </div>
    );
}

export const TextArea = memo(CustomTextArea);
