import { memo } from 'react';

import styles from './input.module.css';

type InputProps = {
    value?: string;
    onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    defaultValue?: string;
    type?: string;
    disabled?: boolean;
    title?: string;
};

function CustomInput({
    value,
    onChange,
    type = 'text',
    disabled = false,
    defaultValue,
    title = '',
}: InputProps) {
    return (
        <div className={styles.wrapper}>
            <div className={styles.title}>{title}</div>
            <input
                value={value}
                defaultValue={defaultValue}
                onChange={onChange}
                type={type}
                disabled={disabled}
                className={`${styles.input}`}
            />
        </div>
    );
}

export const Input = memo(CustomInput);
