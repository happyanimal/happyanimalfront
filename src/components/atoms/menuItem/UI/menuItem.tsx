import styles from './menuItem.module.css';

type MenuItemProps = {
    text?: string;
    img?: string;
    active?: boolean;
};

export function MenuItem({ text, img, active = false }: MenuItemProps) {
    return (
        <div
            className={
                active ? `${styles.wrapper} ${styles.active}` : styles.wrapper
            }
        >
            {img && <img src={img} alt="img" className={styles.img} />}
            <div className={`${styles.text} menuTitle`}>{text}</div>
        </div>
    );
}
