import { setNotification } from 'components/molecules/notifications/reducers/notification';
import { useAppDispatch } from 'hooks/defaulthooks';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import * as yup from 'yup';

import { useAddUserMutation } from '../requests/requests';

const schema = yup.object().shape({
    name: yup.string().required(),
    lastName: yup.string().required(),
    patronymic: yup.string().required(),
    age: yup.string().required(),
    userRole: yup.string(),
    login: yup.string(),
    password: yup.string().required(),
});

const useAddEmployeePage = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [patronymic, setPatronymic] = useState('');
    const [userRole, setUserRole] = useState('');
    const [age, setAge] = useState('');
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    const handleChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value);
    };
    const handleChangeLastName = (e: React.ChangeEvent<HTMLInputElement>) => {
        setLastName(e.target.value);
    };
    const handleChangePatronymic = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPatronymic(e.target.value);
    };
    const handleChangeLogin = (e: React.ChangeEvent<HTMLInputElement>) => {
        setLogin(e.target.value);
    };
    const handleChangeAge = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAge(e.target.value);
    };
    const handleChangePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(e.target.value);
    };
    const handleChangeUserRole = (value: string | number) => {
        setUserRole(`${value}`);
    };

    const [addUser, params] = useAddUserMutation();

    const handleAddUser = (e: any) => {
        e.preventDefault();
        schema
            .validate({
                name,
                lastName,
                patronymic,
                age,
                userRole,
                login,
                password,
            })
            .then(async (valid) => {
                if (valid) {
                    await addUser({
                        age: Number(age),
                        name,
                        lastName,
                        patronymic,
                        userRole,
                        login,
                        password,
                    });
                }
            });
    };

    useEffect(() => {
        if (params.isSuccess) {
            dispatch(
                setNotification({
                    type: 'success',
                    text: `Сотрудник успешно создан`,
                    isActive: true,
                })
            );
            navigate('/employees');
        }
    });

    return {
        name,
        lastName,
        patronymic,
        age,
        userRole,
        login,
        password,
        handleChangeName,
        handleChangeLastName,
        handleChangePatronymic,
        handleChangeAge,
        handleChangeLogin,
        handleChangePassword,
        handleChangeUserRole,
        handleAddUser,
    };
};

export default useAddEmployeePage;
