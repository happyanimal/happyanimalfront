import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        addUser: build.mutation({
            query: (params) => ({
                url: `/api/users`,
                method: 'POST',
                body: {
                    ...params,
                },
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useAddUserMutation } = extendedApi;
