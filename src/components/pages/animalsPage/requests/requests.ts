import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getAnimals: build.query({
            query: ({ page, size }) => ({
                url: `api/animals`,
                method: 'GET',
                params: {
                    page,
                    size,
                },
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useGetAnimalsQuery } = extendedApi;
