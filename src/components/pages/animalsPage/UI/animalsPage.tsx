import Pagination from '@mui/material/Pagination';
import { AnimalBlock } from 'components/molecules/animalBlock/UI/animalBlock';
import { AddAndOptionButtons } from 'components/organisms/addAndOptionButtons/UI/addAndOptionButtons';

import addImg from '../assets/add.svg';
import useAnimalsPage from '../logic/useAnimalsPage';
import styles from './animalsPage.module.css';

export default function AnimalsPage() {
    const { animals, currentPage, handleAddAnimal, handleChangePage, role } =
        useAnimalsPage();
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>Животные</div>
                {role === 'ADMIN' && (
                    <AddAndOptionButtons
                        addImg={addImg}
                        handleAdd={handleAddAnimal}
                    />
                )}
                <div className={styles.wrapperAnimals}>
                    {animals?.content?.map((el: any) => (
                        <AnimalBlock
                            link={`${el.id}`}
                            key={`AnimalBlock${el.id}`}
                            name={el.name}
                            gender={el.gender}
                            status={el.status}
                            img={el.animalKindDto?.pic}
                        />
                    ))}
                </div>
                <div className={styles.wrapperPagination}>
                    <Pagination
                        count={animals?.totalPages}
                        page={currentPage + 1}
                        onChange={handleChangePage}
                    />
                </div>
            </div>
        </div>
    );
}
