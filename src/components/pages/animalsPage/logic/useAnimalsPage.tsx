import { useState } from 'react';
import { useNavigate } from 'react-router';

import { useGetAnimalsQuery } from '../requests/requests';

const useAnimalsPage = () => {
    const navigate = useNavigate();
    const handleAddAnimal = () => {
        navigate('/animals/add');
    };

    const [currentPage, setCurrentPage] = useState(0);
    const handleChangePage = (event: any, pageNumber: number) => {
        setCurrentPage(pageNumber - 1);
    };
    const { data: animals } = useGetAnimalsQuery<any>(
        {
            page: currentPage,
            size: 9,
        },
        { refetchOnMountOrArgChange: true }
    );
    const role = localStorage.getItem('userRole');
    return { animals, currentPage, handleAddAnimal, handleChangePage, role };
};

export default useAnimalsPage;
