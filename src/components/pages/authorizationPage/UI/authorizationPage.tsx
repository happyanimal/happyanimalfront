import { setUserAuthecationInfo } from 'components/molecules/authorizationBlock/reducers/authorization';
import AuthorizationBlock from 'components/molecules/authorizationBlock/UI/authorizationBlock';
import { useAppDispatch, useAppSelector } from 'hooks/defaulthooks';
import { useEffect } from 'react';

import { setIsAuthorization } from '../reducers/authorization';

type AuthorizationPageProps = {
    children: any;
};

export default function AuthorizationPage({
    children,
}: AuthorizationPageProps) {
    const dispatch = useAppDispatch();
    const tokenStore = useAppSelector(
        (state) => state.authenticatedUserInfo.token
    );
    useEffect(() => {
        const token = localStorage.getItem('token');
        if (token) {
            dispatch(setUserAuthecationInfo({ token }));
        }
    }, [tokenStore]);

    if (tokenStore) {
        return children;
    }
    return <AuthorizationBlock />;
}
