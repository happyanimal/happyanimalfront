import { AnimalCard } from 'components/organisms/animalCard/UI/animalCard';

import useAnimalCardPage from '../logic/useAnimalCardPage';
import styles from './animalCardPage.module.css';

export default function AnimalCardPage() {
    const {
        name,
        location,
        gender,
        isEdit,
        employee,
        height,
        weight,
        age,
        status,
        featuresOfKeeping,
        externalFeatures,
        isOpen,
        dateForInspections,
        handleClose,
        handleOpen,
        handleAddInspections,
        handleChangeDateForInspections,
        handleChangeHeight,
        handleChangeEmployee,
        handleChangeGender,
        handleChangeName,
        handleChangeLocation,
        handleChangeWeight,
        handleChangeStatus,
        handleChangeAge,
        handleChangeFeaturesOfKeeping,
        handleChangeExternalFeatures,
        handleChangeAnimal,
        handleSellAnimal,
        handleChangeIsEditable,
    } = useAnimalCardPage();
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>
                    Карточка животного
                </div>
                <AnimalCard
                    name={name}
                    location={location}
                    gender={gender}
                    isEdit={isEdit}
                    employee={employee}
                    height={height}
                    weight={weight}
                    age={age}
                    status={status}
                    featuresOfKeeping={featuresOfKeeping}
                    externalFeatures={externalFeatures}
                    isOpen={isOpen}
                    dateForInspections={dateForInspections}
                    handleClose={handleClose}
                    handleOpen={handleOpen}
                    handleAddInspections={handleAddInspections}
                    handleChangeDateForInspections={
                        handleChangeDateForInspections
                    }
                    handleChangeIsEditable={handleChangeIsEditable}
                    handleChangeHeight={handleChangeHeight}
                    handleChangeEmployee={handleChangeEmployee}
                    handleChangeGender={handleChangeGender}
                    handleChangeName={handleChangeName}
                    handleChangeLocation={handleChangeLocation}
                    handleChangeWeight={handleChangeWeight}
                    handleChangeStatus={handleChangeStatus}
                    handleChangeAge={handleChangeAge}
                    handleChangeFeaturesOfKeeping={
                        handleChangeFeaturesOfKeeping
                    }
                    handleChangeExternalFeatures={handleChangeExternalFeatures}
                    handleChangeAnimal={handleChangeAnimal}
                    handleSellAnimal={handleSellAnimal}
                />
            </div>
        </div>
    );
}
