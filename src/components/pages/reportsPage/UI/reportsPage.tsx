import { Report } from 'components/organisms/report/UI/report';

import styles from './reportsPage.module.css';

export default function ReportsPage() {
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>Отчеты</div>
                <Report />
            </div>
        </div>
    );
}
