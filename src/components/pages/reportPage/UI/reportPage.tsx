import { CardReport } from 'components/organisms/cardReport/UI/cardReport';

import styles from './reportPage.module.css';

export default function ReportPage() {
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <CardReport />
            </div>
        </div>
    );
}
