import Pagination from '@mui/material/Pagination';
import { CardlBlock } from 'components/molecules/cardBlock/UI/cardBlock';

import useTasksPage from '../logic/useTaskPage';
import styles from './tasksPage.module.css';

const menuTitles = ['Задача', 'Статус', 'Тип повторения', 'Животное'];

export default function TasksPage() {
    const { tasks, currentPage, handleChangePage } = useTasksPage();
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>Задачи</div>
                <div className={styles.wrapperAnimals}>
                    {tasks?.content?.map((el: any) => (
                        <CardlBlock
                            key={`CardBlockInTasksPage${el.id}`}
                            link={`/tasks/${el.id}`}
                            menuTitles={menuTitles}
                            subtitles={[
                                el.type,
                                el.completed ? 'Выполнено' : 'Не выполнено',
                                el.repeatType,
                                el.animalName,
                            ]}
                            theme="darkGreen"
                        />
                    ))}
                </div>
                <div className={styles.wrapperPagination}>
                    <Pagination
                        count={tasks?.totalPages}
                        page={currentPage + 1}
                        onChange={handleChangePage}
                    />
                </div>
            </div>
        </div>
    );
}
