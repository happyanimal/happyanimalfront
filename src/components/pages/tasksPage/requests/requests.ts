import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getTasks: build.query({
            query: ({ page, size }) => ({
                url: `api/tasks`,
                method: 'GET',
                params: {
                    page,
                    size,
                },
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useGetTasksQuery } = extendedApi;
