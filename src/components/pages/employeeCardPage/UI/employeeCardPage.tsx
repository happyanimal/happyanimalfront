import { EmployeeCard } from 'components/organisms/employeeCard/UI/employeeCard';

import styles from './employeeCardPage.module.css';

export default function EmployeeCardPage() {
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>
                    Карточка сотрудника
                </div>
                <EmployeeCard />
            </div>
        </div>
    );
}
