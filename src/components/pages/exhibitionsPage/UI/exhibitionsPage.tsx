import { ExhibitionsCard } from 'components/organisms/exhibitionsCard/UI/exhibitionsCard';

import styles from './exhibitionsPage.module.css';

export default function ExhibitionsPage() {
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>Выставки</div>
                <ExhibitionsCard />
            </div>
        </div>
    );
}
