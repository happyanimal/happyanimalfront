import { useState } from 'react';
import { useNavigate } from 'react-router';

import { roles } from '../../../../utils/types';
import { useGetUsersQuery } from '../requests/requests';

const useEmployeesPage = () => {
    const navigate = useNavigate();
    const handleAddEmployee = () => {
        navigate('/employees/add');
    };

    const [currentPage, setCurrentPage] = useState(0);
    const handleChangePage = (event: any, pageNumber: number) => {
        setCurrentPage(pageNumber - 1);
    };

    const { data: users } = useGetUsersQuery<any>(
        {
            page: currentPage,
            size: 9,
        },
        { refetchOnMountOrArgChange: true }
    );
    return { users, roles, currentPage, handleChangePage, handleAddEmployee };
};

export default useEmployeesPage;
