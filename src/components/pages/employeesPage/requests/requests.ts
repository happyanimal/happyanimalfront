import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getUsers: build.query({
            query: ({ page, size }) => ({
                url: `api/users`,
                method: 'GET',
                params: {
                    page,
                    size,
                },
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useGetUsersQuery } = extendedApi;
