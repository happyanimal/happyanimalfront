import Pagination from '@mui/material/Pagination';
import { CardlBlock } from 'components/molecules/cardBlock/UI/cardBlock';
import { AddAndOptionButtons } from 'components/organisms/addAndOptionButtons/UI/addAndOptionButtons';

import addImg from '../assets/add.svg';
import useEmployeesPage from '../logic/useEmployeesPage';
import styles from './employeesPage.module.css';

const menuTitles = ['Имя', 'Возраст', 'Должность'];

export default function EmployeesPage() {
    const { users, roles, currentPage, handleChangePage, handleAddEmployee } =
        useEmployeesPage();
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>Сотрудники</div>
                <AddAndOptionButtons
                    addImg={addImg}
                    handleAdd={handleAddEmployee}
                />
                <div className={styles.wrapperAnimals}>
                    {users?.content?.map((el: any) => (
                        <CardlBlock
                            id={el.id}
                            key={`EmployeeBlock${el.id}`}
                            link={`/employees/${el.id}`}
                            menuTitles={menuTitles}
                            subtitles={[
                                `${el.lastName} ${el.name}`,
                                el.age,
                                roles[el.userRole as keyof typeof roles],
                            ]}
                        />
                    ))}
                </div>
                <div className={styles.wrapperPagination}>
                    <Pagination
                        count={users?.totalPages}
                        page={currentPage + 1}
                        onChange={handleChangePage}
                    />
                </div>
            </div>
        </div>
    );
}
