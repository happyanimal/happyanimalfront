import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        addTask: build.mutation({
            query: (params) => ({
                url: `/api/tasks`,
                method: 'POST',
                body: {
                    ...params,
                },
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useAddTaskMutation } = extendedApi;
