import { AddTaskCard } from 'components/organisms/addTaskCard/UI/addTaskCard';

import useTaskPage from '../logic/useTaskPage';
import styles from './addTaskPage.module.css';

export default function AddTaskPage() {
    const {
        date,
        handleChangeDate,
        type,
        repeatCount,
        note,
        handleChangeType,
        handleChangeRepeatCount,
        handleChangeNote,
        handleAddTask,
    } = useTaskPage();
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>Добавление задачи</div>
                <AddTaskCard
                    note={note}
                    handleChangeNote={handleChangeNote}
                    date={date}
                    handleChangeDate={handleChangeDate}
                    type={type}
                    repeatCount={repeatCount}
                    handleChangeType={handleChangeType}
                    handleChangeRepeatCount={handleChangeRepeatCount}
                    handleAddTask={handleAddTask}
                />
            </div>
        </div>
    );
}
