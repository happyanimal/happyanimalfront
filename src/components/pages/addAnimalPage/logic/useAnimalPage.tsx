import { setNotification } from 'components/molecules/notifications/reducers/notification';
import { useAppDispatch } from 'hooks/defaulthooks';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import * as yup from 'yup';

import {
    useAddAnimalMutation,
    useLazyGetObjectWithKindQuery,
} from '../requests/requests';

const schema = yup.object().shape({
    age: yup.string().required(),
    externalFeatures: yup.string(),
    featuresOfKeeping: yup.string(),
    gender: yup.string().required(),
    height: yup.string().required(),
    kind: yup.string().required(),
    name: yup.string().required(),
    status: yup.string().required(),
    userId: yup.string(),
    weight: yup.string().required(),
});

const useAnimalPage = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [name, setName] = useState('');
    const [gender, setGender] = useState('');
    const [employee, setEmployee] = useState('');
    const [height, setHeight] = useState('');
    const [weight, setWeight] = useState('');
    const [location, setLocation] = useState('');
    const [status, setStatus] = useState('');
    const [age, setAge] = useState('');
    const [kind, setKind] = useState('');
    const [animalClass, setAnimalClass] = useState('');
    const [squad, setSquad] = useState('');
    const [featuresOfKeeping, setFeaturesOfKeeping] = useState('');
    const [externalFeatures, setExternalFeatures] = useState('');

    const handleChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value);
    };
    const handleChangeHeight = (e: React.ChangeEvent<HTMLInputElement>) => {
        setHeight(e.target.value);
    };
    const handleChangeWeight = (e: React.ChangeEvent<HTMLInputElement>) => {
        setWeight(e.target.value);
    };
    const handleChangeLocation = (e: React.ChangeEvent<HTMLInputElement>) => {
        setLocation(e.target.value);
    };
    const handleChangeAge = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAge(e.target.value);
    };
    const handleChangeGender = (value: string | number) => {
        setGender(`${value}`);
    };
    const handleChangeEmployee = (value: string | number) => {
        setEmployee(`${value}`);
    };
    const handleChangeStatus = (value: string | number) => {
        setStatus(`${value}`);
    };
    const handleChangeFeaturesOfKeeping = (
        e: React.ChangeEvent<HTMLInputElement>
    ) => {
        setFeaturesOfKeeping(e.target.value);
    };
    const handleChangeExternalFeatures = (
        e: React.ChangeEvent<HTMLInputElement>
    ) => {
        setExternalFeatures(e.target.value);
    };

    const [trigger, params] = useLazyGetObjectWithKindQuery();
    const setClassAndSquad = async (kindName: string) => {
        const result = await trigger(kindName);
        setSquad(result?.data?.squad);
        setAnimalClass(result?.data?.animalClass);
    };
    const handleChangeKind = (value: string | number) => {
        setKind(`${value}`);
        setClassAndSquad(`${value}`);
    };

    const [addAnimal, paramsAdding] = useAddAnimalMutation();

    const handleAddAnimal = (e: any) => {
        e.preventDefault();
        schema
            .validate({
                age,
                externalFeatures,
                featuresOfKeeping,
                gender,
                height,
                kind,
                name,
                status,
                userId: employee,
                weight,
            })
            .then(async (valid) => {
                if (valid) {
                    await addAnimal({
                        age: Number(age),
                        externalFeatures,
                        featuresOfKeeping,
                        gender,
                        height,
                        kind,
                        name,
                        status,
                        userId: employee || 0,
                        weight: Number(weight),
                        location,
                    });
                }
            })
            .catch((err: any) => {
                dispatch(
                    setNotification({
                        type: 'error',
                        text: `Заполните поля для добавления животного`,
                        isActive: true,
                    })
                );
            });
    };

    useEffect(() => {
        if (paramsAdding.isSuccess) {
            dispatch(
                setNotification({
                    type: 'success',
                    text: `Животное успешно добавлено!`,
                    isActive: true,
                })
            );
            navigate(`/animals/${paramsAdding.data.id}`);
        }
    }, [paramsAdding]);

    return {
        externalFeatures,
        featuresOfKeeping,
        employee,
        name,
        gender,
        height,
        weight,
        location,
        status,
        age,
        kind,
        animalClass,
        squad,
        handleChangeHeight,
        handleChangeEmployee,
        handleChangeGender,
        handleChangeName,
        handleChangeLocation,
        handleChangeWeight,
        handleChangeStatus,
        handleChangeAge,
        handleChangeKind,
        handleAddAnimal,
        handleChangeFeaturesOfKeeping,
        handleChangeExternalFeatures,
    };
};

export default useAnimalPage;
