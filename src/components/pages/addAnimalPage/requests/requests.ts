import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getObjectWithKind: build.query({
            query: (kindName) => ({
                url: `api/animalKinds/${kindName}`,
                method: 'GET',
            }),
        }),
        addAnimal: build.mutation({
            query: (params) => ({
                url: `/api/animals`,
                method: 'POST',
                body: {
                    ...params,
                },
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useLazyGetObjectWithKindQuery, useAddAnimalMutation } =
    extendedApi;
