import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getTask: build.query({
            query: (id) => ({
                url: `/api/tasks/${id}`,
                method: 'GET',
            }),
        }),
        changeTask: build.mutation({
            query: ({ id, ...params }) => ({
                url: `/api/tasks/${id}`,
                method: 'PUT',
                body: {
                    ...params,
                },
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useLazyGetTaskQuery, useChangeTaskMutation } = extendedApi;
