import './index.css';
import 'leaflet/dist/leaflet.css';

import DayJsUtils from '@date-io/dayjs';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { ThemeProvider } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { Header } from 'components/organisms/header/UI/header';
import AuthorizationPage from 'components/pages/authorizationPage/UI/authorizationPage';
import dayjs from 'dayjs';
import ruRU from 'dayjs/locale/ru';
import utc from 'dayjs/plugin/utc';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { RoutesPaths } from 'Router/Router';

import App from './App';
import { store } from './store/store';
import { theme } from './utils/theme';

dayjs.extend(utc);

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <LocalizationProvider dateAdapter={AdapterDayjs} locale={ruRU}>
                    <ThemeProvider theme={theme}>
                        <MuiPickersUtilsProvider
                            utils={DayJsUtils}
                            locale={ruRU}
                        >
                            <AuthorizationPage>
                                <Header />
                                <RoutesPaths />
                            </AuthorizationPage>
                            <App />
                        </MuiPickersUtilsProvider>
                    </ThemeProvider>
                </LocalizationProvider>
            </BrowserRouter>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);
